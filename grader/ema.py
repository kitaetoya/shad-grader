from . import task
import shutil
import pathlib


class EmaTask(task.Task):
    def __init__(self, name, **kwargs):
        super().__init__(name, **kwargs)

    def grade(self, submit_root):
        self.copy_sources(submit_root)

        self.check_call(["../private/{}/test.sh".format(self.name), submit_root],
                        cwd=str(self.root / self.name),
                        timeout=10800)
