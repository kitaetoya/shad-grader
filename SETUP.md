# Добавление нового курса

 1. Создать публичный репозиторий на gitlab.com. Пример названия prime/shad-cpp. Дать доступ slon@.
 2. Создать приватный репозиторий на gitlab.com. Дать доступ slon@.
 3. Создать gdoc документ, с колонками как у https://docs.google.com/spreadsheets/d/1HFhqTdXMe3b_zwpke3ULguqeZSLA1uKjNjyxJV-lUFc/edit#gid=0
    Дать доступ на запись grader@shad-ts.iam.gserviceaccount.com и dartslon@gmail.com. 
 4. Зарегистрироваться на gitlab.manytask.org. Можно сделать это через https://cpp.manytask.org (пароль ilovecpp).
